country_decisions = {
	decision_display_artifice_capacity_breakdown = {
		major = yes
	
		potential = {
			has_estate = estate_artificers
		}
		
		allow = {
			ai = no
		}
	
		effect = {
			custom_tooltip = artifice_press_decision_to_update_tt
			tooltip = {
				artifice_breakdown = yes
			}
			country_event = {
				id = anb_debug.100
			}
		}
	}
}