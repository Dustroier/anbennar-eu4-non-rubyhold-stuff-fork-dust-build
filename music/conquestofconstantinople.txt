# Conquest of Constantinople
# Three songs, exclusively for Byzantine or Ottoman rulers
# Made to play for Bulwari

song = {
	name = "atthegatesofconstantinople"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { bulwar_superregion = { has_discovered = ROOT } }
		}
		modifier = {
			factor = 2
			capital_scope = { superregion = bulwar_superregion }
		}
	}
}

song = {
	name = "thecaravan"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { bulwar_superregion = { has_discovered = ROOT } }
		}
		modifier = {
			factor = 2
			capital_scope = { superregion = bulwar_superregion }
		}
	}
}

song = {
	name = "welcometoconstantinople"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { bulwar_superregion = { has_discovered = ROOT } }
		}
		modifier = {
			factor = 2
			capital_scope = { superregion = bulwar_superregion }
		}
	}
}

