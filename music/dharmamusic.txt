# Dharma Music Pack
# Three songs, For Playing in and around India
# Rahen and Taychend

song = {
	name = "carnaticmusic"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = {
				any_owned_province = { superregion = rahen_superregion }
				any_owned_province = { superregion = greater_taychend_superregion }
			}
		}
		modifier = {
			factor = 2
			capital_scope = {
				superregion = rahen_superregion
				superregion = greater_taychend_superregion
			}
		}
		modifier = {
			factor = 1.5
			culture_group = taychendi_ruinborn_elf
		}
	}
}

song = {
	name = "hindustanimusic"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = {
				any_owned_province = { superregion = rahen_superregion }
				any_owned_province = { superregion = greater_taychend_superregion }
			}
		}
		modifier = {
			factor = 2
			capital_scope = {
				superregion = rahen_superregion
				superregion = greater_taychend_superregion
			}
		}
	}
}

song = {
	name = "rajastanimusic"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = {
				any_owned_province = { superregion = rahen_superregion }
				any_owned_province = { superregion = greater_taychend_superregion }
			}
		}
		modifier = {
			factor = 2
			capital_scope = {
				superregion = rahen_superregion
				superregion = greater_taychend_superregion
			}
		}
	}
}

